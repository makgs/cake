-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 15 2015 г., 09:36
-- Версия сервера: 5.5.45
-- Версия PHP: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `personalhomepage`
--

-- --------------------------------------------------------

--
-- Структура таблицы `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `message` varchar(500) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `message`, `created`, `modified`, `user_id`) VALUES
(1, 'max', 'max.shipitko@gmail.com', 'first post', '2015-10-12 10:33:29', '2015-10-12 10:33:29', NULL),
(2, 'name', 'email@simple.ru', 'second message', '2015-10-12 10:35:28', '2015-10-12 10:35:28', NULL),
(5, 'qwerty', 'qw@er.ty', 'q w e r t y', '2015-10-12 13:38:58', '2015-10-12 13:38:58', 668),
(6, 'mahatma gandhi', 'mahatma.gandhi@gmail.com', 'Открытое разногласие — часто — признак движения вперёд.', '2015-10-15 08:54:27', '2015-10-15 08:54:27', 668),
(7, 'polly', 'polly@mail.ru', 'polly wants a cracker', '2015-10-15 09:18:00', '2015-10-15 09:18:00', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
