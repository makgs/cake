    <div class="container">
      <div class="starter-template">
        <table>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Message</th>
                <th>Created</th>
                <th></th>
            </tr>

        <?php foreach ($contacts as $contact): ?>
            <tr>
                <td><?php echo $contact['Admin']['id']; ?></td>
                <td><?php echo $contact['Admin']['name']; ?></td>
                <td><?php echo $contact['Admin']['email']; ?></td>
                <td><?php echo $contact['Admin']['message']; ?></td>
                <td><?php echo $contact['Admin']['created']; ?></td>
                <td>
                    <?php
                        echo $this->Form->postLink(
                            'Delete',
                            array('action' => 'delete', $contact['Admin']['id']),
                            array('confirm' => 'Are you sure?')
                        );
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>

        </table>
    </div>
</div><!-- /.container -->