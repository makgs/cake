<?php

	App::uses('AppController', 'Controller');

	class AdminsController extends AppController {
		
		public function index() {
			$this->set('contacts', $this->Admin->find('all'));
		}
		
		public function view() {
			$this->set('contacts', $this->Admin->find('all'));
		}
		
		public function beforeFilter() {
			parent::beforeFilter();
			$this->Auth->allow('login');
            $this->layout = 'admin';
		}
		
		public function delete($id) {
			$this->Admin->delete($id);
			return $this->redirect(array('action' => 'index'));
		}
		
		
		
	}