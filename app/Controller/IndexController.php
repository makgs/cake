<?php
	class IndexController extends AppController {
		public function index() {

		}
		
		public function beforeFilter() {
			parent::beforeFilter();
			// Allow users to register and logout.
			$this->Auth->allow('add', 'index');
		}
	}