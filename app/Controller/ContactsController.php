<?php
class ContactsController extends AppController {
	
	public $helpers = array('Html', 'Form');

    public function index() {
        $this->set('contacts', $this->Contact->find('all'));
		$this->redirect(array('action' => 'add'));
    }

    public function add() {
        if ($this->request->is('post')) {
			//Added this line
			$this->request->data['Contact']['user_id'] = $this->Auth->user('id');
			if ($this->Contact->save($this->request->data)) {
				$this->Flash->success(__('Your contacts has been saved.'));
				//return $this->redirect(array('action' => 'index'));
			}
		}
    }
	
	public function beforeFilter() {
		parent::beforeFilter();
		// Allow users to register and logout.
		$this->Auth->allow('add', 'index');
	}

}